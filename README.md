Projeto base para um sistema de login.

O projeto já a configuração para os containeres docker necessários para execução do projeto.

O ambiente utilizado no projeto será:

- PHP 7.0 com mod_rewrite ativo (arquivo `[optional].htaccess` disponibilizado).
- MySQL 5.7
- PHPMyAdmin 

Para utilizar o ambiente deve-se ter o docker e docker compose instalados. (https://docs.docker.com/engine/install/ubuntu/)

Executar na pasta raiz do projeto o comando: docker-compose up

O código fonte deve ser adicionado dentro de `/src` e para acessar no navegador deve utilizar a URL: http://localhost:8100/

Para acessar o PHPMyAdmin deve-se acessar a URL: http://localhost:8080/

O usuário MySQL é `aula` e a senha `aula` 
